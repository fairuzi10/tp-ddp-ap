public class TrainCar {
    public static final double EMPTY_WEIGHT = 20; // In kilograms
    public WildCat cat;
    public TrainCar next;
    public static TrainCar last;

    public TrainCar(WildCat cat) {
        this.cat = cat;
        this.last = this;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
        this.last = this;
    }

    public double computeTotalWeight() {
        double weight = TrainCar.EMPTY_WEIGHT + this.cat.weight;
        if (this.next != null) {
            weight += next.computeTotalWeight();
        }
        return weight;
    }

    public double computeTotalMassIndex() {
        double totalMassIndex = this.cat.computeMassIndex();
        if (this.next != null) {
            totalMassIndex += this.next.computeTotalMassIndex();
        }
        return totalMassIndex;
    }

    public void printCar() {
        System.out.format("(%s)", this.cat.name);
        if (this.next != null) {
            System.out.print("--");
            this.next.printCar();
        } else {
            System.out.println();
        }
    }
}
