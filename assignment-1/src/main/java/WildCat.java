public class WildCat {
    public String name;
    public double weight; // In kilograms
    public double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public double computeMassIndex() {
        return this.weight * 10000 / (this.length * this.length);
    }
}
