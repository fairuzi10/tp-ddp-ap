import java.util.Scanner;

public class A1Station {
    private static final double THRESHOLD = 250; // in kilograms

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        int done = -1;
        for (int i = 0; i < n; i++) {
            String masukanString = input.next();
            String[] masukanArray = masukanString.split(",");
            WildCat kucingBaru = new WildCat(masukanArray[0], Double.parseDouble(masukanArray[1]),
                                             Double.parseDouble(masukanArray[2]));
            if (TrainCar.last == null) {
                new TrainCar(kucingBaru);
            } else {
                new TrainCar(kucingBaru, TrainCar.last);
            }

            if (TrainCar.last.computeTotalWeight() > A1Station.THRESHOLD || i == n - 1) {
                System.out.println("The train departs to Javari Park");
                System.out.print("[LOCO]<--");
                TrainCar.last.printCar();

                double averageMassIndex = TrainCar.last.computeTotalMassIndex() / (i - done);
                System.out.format("Average mass index of all cats: %.2f%n", averageMassIndex);
                System.out.print("In average, the cats in the train are *");
                if (averageMassIndex < 18.5) {
                    System.out.print("underweight");
                } else if (averageMassIndex < 25) {
                    System.out.print("normal");
                } else if (averageMassIndex < 30) {
                    System.out.print("overweight");
                } else {
                    System.out.print("obese");
                }
                System.out.println("*");
                TrainCar.last = null;
                done = i;
            }
        }
    }
}
