package animal;

public class Hamster extends Animal {
    public Hamster(String nama, Integer panjang) {
        super(nama, panjang);
        setActionList(new String[]{"See it gnawing", "Order to run in the hamster wheel"});
    }

    public void perform(int i) {
        String suara[] = {"ngkkrit.. ngkkrrriiit", "Trrr... Trrr..."};
        System.out.printf("%s makes a voice: %s\n", getNama(), suara[i-1]);
        System.out.println("Back to the office!");
    }
}
