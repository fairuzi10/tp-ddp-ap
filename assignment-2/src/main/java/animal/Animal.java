package animal;

public abstract class Animal {
    private String nama;
    private int panjang;
    private String[] actionList;

    public Animal(String nama, Integer panjang) {
        this.nama = nama;
        this.panjang = panjang;
    }

    public void setActionList(String[] actionList) {
        this.actionList = actionList;
    }

    public String getNama() {
        return nama;
    }

    public int getPanjang() {
        return panjang;
    }

    public abstract void perform(int i);

    public void printActionList() {
        for (int i = 0; i < actionList.length; i++) {
            System.out.printf("%d: %s ", i + 1, actionList[i]);
        }
        System.out.println();
    }
}