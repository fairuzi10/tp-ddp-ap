package animal;

import java.util.Scanner;

public class Parrot extends Animal {
    public Parrot(String nama, Integer panjang) {
        super(nama, panjang);
        setActionList(new String[]{"Order to fly", "Do conversation"});
    }
    public void perform(int i) {
        if (i == 1) {
            System.out.printf("%s makes a voice: FLYYYY...\n", getNama());
        } else if (i == 2) {
            Scanner in = new Scanner(System.in);
            System.out.print("You say: ");
            String kata = in.nextLine();
            System.out.printf("%s says: %s\n", getNama(), kata.toUpperCase());
        } else {
            System.out.printf("%s says: HM?\n", getNama());
        }
        System.out.println("Back to the office!");
    }
}
