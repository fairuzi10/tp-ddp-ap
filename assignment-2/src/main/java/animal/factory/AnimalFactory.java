package animal.factory;

import animal.Animal;

public interface AnimalFactory {
    Animal create(String nama, int panjang);
}
