package animal.factory;

import animal.Animal;
import animal.Hamster;

public class HamsterFactory implements AnimalFactory {
    public Animal create(String nama, int panjang) {
        return new Hamster(nama, panjang);
    }
}
