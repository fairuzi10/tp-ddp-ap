package animal.factory;

import animal.Animal;
import animal.Eagle;

public class EagleFactory implements AnimalFactory {
    public Animal create(String nama, int panjang) {
        return new Eagle(nama, panjang);
    }
}
