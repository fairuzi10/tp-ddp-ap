package animal.factory;

import animal.Animal;
import animal.Lion;

public class LionFactory implements AnimalFactory {
    public Animal create(String nama, int panjang) {
        return new Lion(nama, panjang);
    }
}
