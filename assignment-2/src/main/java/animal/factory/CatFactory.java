package animal.factory;

import animal.Animal;
import animal.Cat;

public class CatFactory implements AnimalFactory {
    public Animal create(String nama, int panjang) {
        return new Cat(nama, panjang);
    }
}
