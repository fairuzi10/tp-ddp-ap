package animal.factory;

import animal.Animal;
import animal.Parrot;

public class ParrotFactory implements AnimalFactory {
    public Animal create(String nama, int panjang) {
        return new Parrot(nama, panjang);
    }
}
