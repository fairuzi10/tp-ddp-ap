package animal;

import java.util.Random;

public class Cat extends Animal {
    public Cat(String nama, Integer panjang) {
        super(nama, panjang);
        setActionList(new String[]{"Brush the fur", "Cuddle"});
    }

    public void perform(int i) {
        String cuddle[] = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
        if (i == 1) {
            System.out.printf("Time to clean %s's fur\n" +
                    "%s makes a voice: Nyaaan...\n", getNama(), getNama());
        } else if (i == 2) {
            int rnd = new Random().nextInt(cuddle.length);
            System.out.printf("%s makes a voice: %s\n", getNama(), cuddle[rnd]);
        }
        System.out.println("Back to the office!");
    }
}
