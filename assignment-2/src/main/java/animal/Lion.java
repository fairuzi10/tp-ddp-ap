package animal;

public class Lion extends Animal {
    public Lion(String nama, Integer panjang) {
        super(nama, panjang);
        setActionList(new String[]{"See it hunting", "Brush the mane", "Disturb it"});
    }

    public void perform(int i) {
        String[] suara = {"err...!", "Hauhhmm!", "HAUHHMM!"};
        if (i == 1) {
            System.out.println("Lion is hunting..");
        } else if (i == 2) {
            System.out.println("Clean the lion’s mane..");
        }
        System.out.printf("%s makes a voice: %s\n", getNama(), suara[i-1]);
        System.out.println("Back to the office!");
    }
}
