package animal;

public class Eagle extends Animal {
    public Eagle(String nama, Integer panjang) {
        super(nama, panjang);
        setActionList(new String[]{"Order to fly"});
    }

    public void perform(int i) {
        if (i == 1) {
            System.out.printf("%s makes a voice: kwaakk...\n", getNama());
            System.out.println("You hurt!");
        } else {
            System.out.println("You do nothing...");
        }
        System.out.println("Back to the office!");
    }
}
