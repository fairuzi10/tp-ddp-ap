import animal.*;
import animal.factory.*;

import java.util.*;

class Zoo {
    private static Map<String, List<List<Cage>>> cages = new HashMap<>();
    private static Map<String, Integer> number = new HashMap<>();
    private static Map<String, Map<String, Animal>> animalList = new HashMap<>();

    private static void input(String nama, AnimalFactory factory) {
        Scanner in = new Scanner(System.in);
        in.useDelimiter("[\\s|,]");
        System.out.printf("%s: ", nama);
        int N = in.nextInt();
        number.put(nama, N);
        animalList.put(nama, new HashMap<>());

        if (N != 0) {
            List<Cage> temp = new ArrayList<>();
            System.out.printf("Provide the information of %s(s):\n", nama);
            for (int i = 0; i < number.get(nama); i++) {
                String namaHewan = in.next();
                int panjang = in.nextInt();
                Animal hewan = factory.create(namaHewan, panjang);
                animalList.get(nama).put(namaHewan, hewan);
                Cage kandang = new Cage(hewan);
                temp.add(kandang);
            }
            cages.put(nama, new ArrayList<>());
            cages.get(nama).add(temp.subList(0, (N + 2) / 3));
            cages.get(nama).add(temp.subList((N + 2) / 3, (N + 2) / 3 + (N + 1) / 3));
            cages.get(nama).add(temp.subList((N + 2) / 3 + (N + 1) / 3, N));
        }
    }

    private static void print(String nama) {
        List<List<Cage>> kandang = cages.get(nama);
        if (kandang != null) {
            System.out.printf("location: %s\n", kandang.get(0).get(0).getTipe());
            for (int i = 2; i >= 0; i--) {
                System.out.printf("level %d: ", i + 1);
                for (int j = 0; j < kandang.get(i).size(); j++) {
                    if (j > 0) {
                        System.out.print(", ");
                    }
                    System.out.print(kandang.get(i).get(j));
                }
                System.out.println();
            }
            System.out.println();

            System.out.println("After rearrangement...");
            for (int i = 2; i >= 0; i--) {
                System.out.printf("level %d: ", i + 1);
                List<Cage> proses = kandang.get((i + 2) % 3);
                for (int j = 0; j < proses.size(); j++) {
                    if (j > 0) {
                        System.out.print(", ");
                    }
                    int idx = proses.size() - 1 - j;
                    System.out.print(proses.get(idx));
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    private static void process(String nama) {
        Map<String, Animal> daftar = animalList.get(nama);
        System.out.printf("Mention the name of %s you want to visit: ", nama);
        Scanner in = new Scanner(System.in);
        String namaHewan = in.next();
        Animal hewan = daftar.get(namaHewan);
        if (hewan == null) {
            System.out.printf("There is no %s with that name! Back to the office!\n", nama);
        } else {
            System.out.printf("You are visiting %s (%s) now, what would you like to do?\n", nama, namaHewan);
            hewan.printActionList();
            int N = in.nextInt();
            hewan.perform(N);
        }
    }

    private static void menu() {
        Scanner in = new Scanner(System.in);
        System.out.println("=============================================\n");
        String[] daftar = {"cat", "eagle", "hamster", "parrot", "lion"};
        while (true) {
            System.out.println("Which animal you want to visit?\n" +
                "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            int N = in.nextInt();
            if (N == 99) {
                break;
            }
            process(daftar[N-1]);
            System.out.println();
        }

    }

    public static void main(String[] args) {
        Animal x = new Cat("kuda", 5);
        System.out.println(x.getClass().getName());
        System.out.println("Welcome to Javari Park!\n" +
            "Input the number of animals");
        input("cat", new CatFactory());
        input("lion", new LionFactory());
        input("eagle", new EagleFactory());
        input("parrot", new ParrotFactory());
        input("hamster", new HamsterFactory());
        System.out.println("Animals have been successfully recorded!\n" +
            "\n" +
            "=============================================\n" +
            "Cage arrangement:");
        print("cat");
        print("lion");
        print("eagle");
        print("parrot");
        print("hamster");

        menu();
    }
}