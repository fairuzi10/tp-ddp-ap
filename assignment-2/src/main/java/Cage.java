import animal.Animal;
import animal.Eagle;
import animal.Lion;

public class Cage {
    private String tipe;
    private char kelas;
    private Animal hewan;

    public String getTipe() {
        return tipe;
    }

    @Override
    public String toString() {
        return String.format("%s (%d - %c)", hewan.getNama(), hewan.getPanjang(), kelas);
    }

    public Cage(Animal animal) {
        hewan = animal;
        if (animal instanceof Eagle || animal instanceof Lion) {
            tipe = "outdoor";
        } else {
            tipe = "indoor";
        }
        int offset = tipe.equals("indoor")? 0: 30;
        int panjang = animal.getPanjang();
        if (panjang < 45+offset) {
            kelas = 'A';
        } else if (panjang < 60+offset) {
            kelas = 'B';
        } else {
            kelas = 'C';
        }
    }
}
