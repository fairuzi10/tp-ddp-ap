package javari.animal;

public class Eagle extends Aves {
    public Eagle(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, boolean layingEgg) {
        super(id, type, name, gender, length, weight, condition, layingEgg);
    }
}