package javari.animal;

public class Aves extends Animal {
    private boolean layingEgg;

    public Aves(Integer id, String type, String name, Gender gender, double length,
                   double weight, Condition condition, boolean layingEgg) {
        super(id, type, name, gender, length, weight, condition);
        this.layingEgg = layingEgg;
    }

    public boolean specificCondition() {
        return !this.layingEgg;
    }
}
