package javari.animal;

public class Mammal extends Animal {
    private boolean pregnant;

    public Mammal(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, boolean pregnant) {
        super(id, type, name, gender, length, weight, condition);
        this.pregnant = pregnant;
    }

    public boolean specificCondition() {
        return !this.pregnant;
    }
}
