package javari.animal;

public class Snake extends Reptile {
    public Snake(Integer id, String type, String name, Gender gender, double length,
                   double weight, Condition condition, boolean wild) {
        super(id, type, name, gender, length, weight, condition, wild);
    }
}
