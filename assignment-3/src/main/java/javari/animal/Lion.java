package javari.animal;

public class Lion extends Mammal {

    public Lion(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, boolean pregnant) {
        super(id, type, name, gender, length, weight, condition, pregnant);
    }

    @Override
    public boolean specificCondition() {
        return super.specificCondition() && getGender() == Gender.MALE;
    }
}
