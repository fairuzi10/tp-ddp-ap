package javari.animal;

public class Reptile extends Animal {
    private boolean wild;

    public Reptile(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, boolean wild) {
        super(id, type, name, gender, length, weight, condition);
        this.wild = wild;
    }

    public boolean specificCondition() {
        return !this.wild;
    }
}
