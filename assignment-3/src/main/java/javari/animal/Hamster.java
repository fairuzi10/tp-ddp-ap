package javari.animal;

public class Hamster extends Mammal {
    public Hamster(Integer id, String type, String name, Gender gender, double length,
                 double weight, Condition condition, boolean pregnant) {
        super(id, type, name, gender, length, weight, condition, pregnant);
    }
}
