package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import javari.animal.*;

public class RecordReader extends CsvReader {
    public RecordReader(Path file) throws IOException {
        super(file.resolve("animals_records.csv"));
    }

    public long countValidRecords() {
        return lines.size();
    }

    public long countInvalidRecords() {
        return 0;
    }

    public List<Animal> getAnimal() {
        List<Animal> result = new ArrayList<>();
        List<String> lines = getLines();
        for (String line: lines) {
            String[] now = line.split(COMMA);
            Animal newAnimal;
            switch (now[1]) {
                case "Cat":
                    newAnimal = new Cat(Integer.parseInt(now[0]), now[1], now[2],
                        Gender.parseGender(now[3]), Double.parseDouble(now[4]),
                        Double.parseDouble(now[5]), Condition.parseCondition(now[7]),
                        now[6].equals("pregnant"));
                    break;
                case "Hamster":
                    newAnimal = new Hamster(Integer.parseInt(now[0]), now[1], now[2],
                        Gender.parseGender(now[3]), Double.parseDouble(now[4]),
                        Double.parseDouble(now[5]), Condition.parseCondition(now[7]),
                        now[6].equals("pregnant"));
                    break;
                case "Lion":
                    newAnimal = new Lion(Integer.parseInt(now[0]), now[1], now[2],
                        Gender.parseGender(now[3]), Double.parseDouble(now[4]),
                        Double.parseDouble(now[5]), Condition.parseCondition(now[7]),
                        now[6].equals("pregnant"));
                    break;
                case "Whale":
                    newAnimal = new Whale(Integer.parseInt(now[0]), now[1], now[2],
                        Gender.parseGender(now[3]), Double.parseDouble(now[4]),
                        Double.parseDouble(now[5]), Condition.parseCondition(now[7]),
                        now[6].equals("pregnant"));
                    break;
                case "Eagle":
                    newAnimal = new Eagle(Integer.parseInt(now[0]), now[1], now[2],
                        Gender.parseGender(now[3]), Double.parseDouble(now[4]),
                        Double.parseDouble(now[5]), Condition.parseCondition(now[7]),
                        now[6].equals("laying eggs"));
                    break;
                case "Parrot":
                    newAnimal = new Parrot(Integer.parseInt(now[0]), now[1], now[2],
                        Gender.parseGender(now[3]), Double.parseDouble(now[4]),
                        Double.parseDouble(now[5]), Condition.parseCondition(now[7]),
                        now[6].equals("laying eggs"));
                    break;
                case "Snake":
                    newAnimal = new Snake(Integer.parseInt(now[0]), now[1], now[2],
                        Gender.parseGender(now[3]), Double.parseDouble(now[4]),
                        Double.parseDouble(now[5]), Condition.parseCondition(now[7]),
                        now[6].equals("wild"));
                    break;
                default:
                    newAnimal = null;
            }
            result.add(newAnimal);
        }
        return result;
    }
}
