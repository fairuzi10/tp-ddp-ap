package javari.reader;

import java.io.IOException;
import java.nio.file.Path;

public class CategoryReader extends CsvReader {

    /**
     * Defines the base constructor for instantiating an object of
     * {@code TypeReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public CategoryReader(Path file) throws IOException {
        super(file.resolve("animals_categories.csv"));
    }

    /**
     * Counts the number of valid type-category-section from read CSV file.
     *
     * @return the number of valid records from read CSV file.
     */
    public long countValidRecords() {
        long result = 0;
        boolean[] exists = new boolean[3];
        for (String line: lines) {
            String[] now = line.split(COMMA);
            String type = now[0];
            String category = now[1];
            String section = now[2];

            switch (type) {
                case "Hamster":
                case "Lion":
                case "Cat":
                case "Whale":
                    if (category.equals("mammals") && section.equals("Explore the Mammals")) {
                        exists[0] = true;
                    }
                    break;
                case "Eagle":
                case "Parrot":
                    if (category.equals("aves") && section.equals("World of Aves")) {
                        exists[1] = true;
                    }
                    break;
                case "Snake":
                    if (category.equals("reptiles") && section.equals("Reptillian Kingdom")) {
                        exists[2] = true;
                    }
                    break;
                default:
                    break;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (exists[i]) {
                result += 1;
            }
        }
        return result;
    }

    public long countInvalidRecords() {
        long result = 0;
        for (String line: lines) {
            String[] now = line.split(COMMA);
            String type = now[0];
            String category = now[1];
            String section = now[2];

            switch (type) {
                case "Hamster":
                case "Lion":
                case "Cat":
                case "Whale":
                    if (!category.equals("mammals") || !section.equals("Explore the Mammals")) {
                        result++;
                    }
                    break;
                case "Eagle":
                case "Parrot":
                    if (!category.equals("aves") || !section.equals("World of Aves")) {
                        result++;
                    }
                    break;
                case "Snake":
                    if (!category.equals("reptiles") || !section.equals("Reptillian Kingdom")) {
                        result++;
                    }
                    break;
                default:
                    break;
            }
        }
        return result;
    }
}
