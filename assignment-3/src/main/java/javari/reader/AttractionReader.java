package javari.reader;

import java.io.IOException;
import java.nio.file.Path;

public class AttractionReader extends CsvReader {

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public AttractionReader(Path file) throws IOException {
        super(file.resolve("animals_attractions.csv"));
    }

    /**
     * Counts the number of valid type-attraction from read CSV file.
     *
     * @return the number of valid records from read CSV file.
     */
    public long countValidRecords() {
        long result = 0;
        boolean[] exists = new boolean[4];
        for (String line: lines) {
            String[] now = line.split(COMMA);
            String type = now[0];
            String attraction = now[1];

            switch (attraction) {
                case "Circles of Fires":
                    switch (type) {
                        case "Lion":
                        case "Whale":
                        case "Eagle":
                            exists[0] = true;
                            break;
                        default:
                            break;
                    }
                    break;
                case "Dancing Animals":
                    switch (type) {
                        case "Parrot":
                        case "Snake":
                        case "Cat":
                        case "Hamster":
                            exists[1] = true;
                            break;
                        default:
                            break;
                    }
                    break;
                case "Counting Masters":
                    switch (type) {
                        case "Hamster":
                        case "Whale":
                        case "Parrot":
                            exists[2] = true;
                            break;
                        default:
                            break;
                    }
                    break;
                case "Passionate Coders":
                    switch (type) {
                        case "Hamster":
                        case "Cat":
                        case "Snake":
                            exists[3] = true;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        for (int i = 0; i < 4; i++) {
            if (exists[i]) {
                result++;
            }
        }
        return result;
    }

    public long countInvalidRecords() {
        long result = 0;
        for (String line: lines) {
            String[] now = line.split(COMMA);
            String type = now[0];
            String attraction = now[1];

            switch (attraction) {
                case "Circle of Fire":
                    switch (type) {
                        case "Lion":
                        case "Whale":
                        case "Eagle":
                            break;
                        default:
                            result++;
                            break;
                    }
                    break;
                case "Dancing Animals":
                    switch (type) {
                        case "Parrot":
                        case "Snake":
                        case "Cat":
                        case "Hamster":
                            break;
                        default:
                            result++;
                            break;
                    }
                    break;
                case "Counting Masters":
                    switch (type) {
                        case "Hamster":
                        case "Whale":
                        case "Parrot":
                            break;
                        default:
                            result++;
                            break;
                    }
                    break;
                case "Passionate Coders":
                    switch (type) {
                        case "Hamster":
                        case "Cat":
                        case "Snake":
                            break;
                        default:
                            result++;
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        return result;
    }
}
