package javari.park;

import java.util.ArrayList;
import java.util.List;

/**
 * This class describes expected behaviours that represents the concept of attraction registration
 * done by a visitor in Javari Park.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author Muhammad Fairuzi Teguh
 */
public class Registration {
    private int id;
    private String name;
    private List<SelectedAttraction> selected;

    public Registration(int id, String name) {
        this.id = id;
        this.name = name;
        this.selected = new ArrayList<>();
    }

    /**
     * Returns the unique ID that associated with visitor's registration
     * in watching an attraction.
     *
     * @return id of visitor
     */
    public int getRegistrationId() {
        return id;
    }

    /**
     * Returns the name of visitor that associated with the registration.
     *
     * @return the name of visitor
     */
    public String getVisitorName() {
        return name;
    }

    /**
     * Returns the list of all attractions that will be watched by the
     * visitor.
     *
     * @return list of all attraction
     */
    public List<SelectedAttraction> getSelectedAttractions() {
        return selected;
    }

    /**
     * Adds a new attraction that will be watched by the visitor.
     *
     * @param selected  the attraction
     * @return {@code true} if the attraction is successfully added into the
     *     list, {@code false} otherwise
     */
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        this.selected.add(selected);
        return true;
    }
}
