package javari.park;

import java.util.ArrayList;
import java.util.List;

import javari.animal.Animal;

/**
 * This class describes expected behaviours that represents the concept of attraction that
 * will be watched by a visitor in Javari Park.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author Muhammad Fairuzi Teguh
 */
public class SelectedAttraction {
    private String name;
    private String type;
    private List<Animal> animals;

    public SelectedAttraction(String name, String type) {
        this.name = name;
        this.type = type;
        this.animals = new ArrayList<>();
    }

    /**
     * Returns the name of attraction.
     *
     * @return the name of attraction
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the type of animal(s) that performing in this attraction.
     *
     * @return the type of animal that  performing in this attraction
     */
    public String getType() {
        return type;
    }

    /**
     * Returns all performers of this attraction.
     *
     * @return all performers of this attraction
     */
    public List<Animal> getPerformers() {
        return new ArrayList<>(animals);
    }

    /**
     * Adds a new animal into the list of performers.
     *
     * @param performer an instance of animal
     * @return {@code true} if the animal is successfully added into list of
     *     performers, {@code false} otherwise
     */
    public boolean addPerformer(Animal performer) {
        animals.add(performer);
        return true;
    }
}
