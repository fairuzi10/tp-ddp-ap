package javari;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringJoiner;
import javari.animal.Animal;
import javari.park.Registration;
import javari.park.SelectedAttraction;
import javari.reader.AttractionReader;
import javari.reader.CategoryReader;
import javari.reader.RecordReader;
import javari.writer.RegistrationWriter;

public class A3Festival {
    private static List<Animal> animals;
    private static List<Registration> regis = new ArrayList<>();

    private static int getNextState(int state) {
        Scanner in = new Scanner(System.in);
        if (in.hasNextInt()) {
            state = 4 * state + in.nextInt() - 1;
        } else {
            state = state / 4;
        }
        System.out.println();
        return state;
    }

    private static Registration getRegistrationOf(String name) {
        for (Registration reg: regis) {
            if (reg.getVisitorName().equals(name)) {
                return reg;
            }
        }
        Registration newRegis = new Registration(regis.size() + 1, name);
        regis.add(newRegis);
        return newRegis;
    }

    private static int animalMenu(String type, String[] actions, int state) {
        List<Animal> performers = new ArrayList<>();
        for (Animal animal: animals) {
            if (animal.getType().equalsIgnoreCase(type) && animal.isShowable()) {
                performers.add(animal);
            }
        }

        if (performers.isEmpty()) {
            System.out.printf("Unfortunately, no %s can perform any attraction, "
                + "please choose other animals\n", type);
            return state / 4;
        }

        String capital = type.substring(0, 1).toUpperCase() + type.substring(1);
        System.out.printf("---%s---\n"
            + "Attractions by %s:\n", capital, capital);
        for (int i = 0; i < actions.length; i++) {
            System.out.printf("%d. %s\n", i + 1, actions[i]);
        }
        System.out.print("Please choose your preferred attractions (type the number): ");
        Scanner in = new Scanner(System.in);
        if (in.hasNextInt()) {
            int index = in.nextInt() - 1;
            SelectedAttraction selected = new SelectedAttraction(actions[index], type);


            System.out.println();
            System.out.println("Wow, one more step,\n"
                + "please let us know your name: ");
            String name = in.next();
            Registration nowRegis = getRegistrationOf(name);
            nowRegis.addSelectedAttraction(selected);

            System.out.println();
            System.out.println("Yeay, final check!\n"
                + "Here is your data, and the attraction you chose:");
            System.out.printf("Name: %s\n", name);
            System.out.printf("Attractions: %s\n", actions[index]);
            StringJoiner join = new StringJoiner(", ");
            for (Animal performer: performers) {
                join.add(performer.getName());
            }
            System.out.printf("with: %s\n\n", join.toString());
            System.out.print("Is the data correct? (Y/N): ");
            String ans = in.next();
            if (ans.equals("Y")) {
                System.out.println();
                System.out.print("Thank you for your interest. "
                    + "Would you like to register to other attractions? (Y/N): ");
                String ans2 = in.next();
                System.out.println();
                try {
                    RegistrationWriter.writeJson(nowRegis, Paths.get(""));
                    System.out.println();
                } catch (Exception e) {
                    String fileName = String.format("registration_%s.json",
                        name.replace(" ", "_"));
                    System.out.println("Failed to write to " + fileName);
                    return 0;
                }
                if (ans2.equals("Y")) {
                    // back to main menu
                    return 1;
                } else {
                    // end program
                    return 0;
                }
            } else {
                return animalMenu(type, actions, state);
            }
        } else {
            return state / 4;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("... Opening default section database from data. ");
        AttractionReader attractionReader;
        CategoryReader categoryReader;
        RecordReader recordReader;
        try {
            String path = System.getProperty("user.dir");
            attractionReader = new AttractionReader(Paths.get(path));
            categoryReader = new CategoryReader(Paths.get(path));
            recordReader = new RecordReader(Paths.get(path));
        } catch (Exception e) {
            System.out.print("... File not found or incorrect file!");
            System.out.println("\n");
            System.out.print("Please provide the source data path: ");
            String path = in.nextLine();
            try {
                attractionReader = new AttractionReader(Paths.get(path));
                categoryReader = new CategoryReader(Paths.get(path));
                recordReader = new RecordReader(Paths.get(path));
            } catch (Exception e2) {
                System.out.print("... File not found or incorrect file!");
                return;
            }
        }
        System.out.println();
        System.out.println("... Loading... Success... System is populating data...\n");
        animals = recordReader.getAnimal();

        System.out.format("Found %d valid sections and %d invalid sections\n",
            categoryReader.countValidRecords(), categoryReader.countInvalidRecords());
        System.out.format("Found %d valid attractions and %d invalid attractions\n",
            attractionReader.countValidRecords(), attractionReader.countInvalidRecords());
        System.out.format("Found %d valid animal categories and %d invalid animal categories\n",
            categoryReader.countValidRecords(), categoryReader.countInvalidRecords());
        System.out.format("Found %d valid animal records and %d invalid animal records\n",
            recordReader.countValidRecords(), recordReader.countInvalidRecords());

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.println("Please answer the questions by typing the number. "
            + "Type # if you want to return to the previous menu\n");

        int state = 1;
        while (state > 0) {
            switch (state) {
                case 1:
                    // main menu
                    System.out.print("Javari Park has 3 sections:\n"
                        + "1. Explore the Mammals\n"
                        + "2. World of Aves\n"
                        + "3. Reptilian Kingdom\n"
                        + "Please choose your preferred section (type the number): ");
                    state = getNextState(state);
                    break;
                case 4:
                    // mammals
                    System.out.print("--Explore the Mammals--\n"
                        + "1. Hamster\n"
                        + "2. Lion\n"
                        + "3. Cat\n"
                        + "4. Whale\n"
                        + "Please choose your preferred animals (type the number): ");
                    state = getNextState(state);
                    break;
                case 5:
                    // aves
                    System.out.print("--World of Aves--\n"
                        + "1. Eagle\n"
                        + "2. Parrot\n"
                        + "Please choose your preferred animals (type the number): ");
                    state = getNextState(state);
                    break;
                case 6:
                    // reptiles
                    System.out.print("--Reptilian Kingdom--\n"
                        + "1. Snake\n"
                        + "Please choose your preferred animals (type the number): ");
                    state = getNextState(state);
                    break;
                case 16:
                    // hamster
                    state = animalMenu("hamster", new String[]{"Dancing Animals"}, state);
                    break;
                case 17:
                    // lion
                    state = animalMenu("lion", new String[]{"Circle of Fire"}, state);
                    break;
                case 18:
                    // cat
                    state = animalMenu("cat",
                        new String[]{"Dancing Animals", "Passionate Coders"}, state);
                    break;
                case 19:
                    // whale
                    state = animalMenu("whale",
                        new String[]{"Circle of Fire", "Counting Masters"}, state);
                    break;
                case 20:
                    // eagle
                    state = animalMenu("eagle",
                        new String[]{"Circle of Fire"}, state);
                    break;
                case 21:
                    // parrot
                    state = animalMenu("parrot",
                        new String[]{"Dancing Animals", "Counting Masters"}, state);
                    break;
                case 24:
                    // snake
                    state = animalMenu("snake",
                        new String[]{"Dancing Animals", "Passionate Coders"}, state);
                    break;
                default:
                    System.out.println("Invalid step");
                    state = 0;
                    break;
            }
        }
    }
}
