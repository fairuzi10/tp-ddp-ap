import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * A modified class which is a button that can "flip".
 */
class Button extends JButton {
    private int state;
    private int number;
    private ImageIcon[] icon;

    Button(int number, ImageIcon cover, ImageIcon reveal, ActionListener panelListener) {
        super(cover);
        this.number = number;
        icon = new ImageIcon[2];
        icon[0] = cover;
        icon[1] = reveal;
        state = 0;
        setIcon(cover);
        setPreferredSize(new Dimension(100, 100));
        addActionListener(e -> {
            toggleImage();
            panelListener.actionPerformed(e);
        });
    }

    void toggleImage() {
        state ^= 1;
        setIcon(icon[state]);
    }

    int getNumber() {
        return this.number;
    }
}
