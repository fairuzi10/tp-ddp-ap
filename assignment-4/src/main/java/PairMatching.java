import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * Main class that control ButtonPanel and other button/text.
 */
public class PairMatching {
    // need to make it static (not local) to use it in lambda function
    private static int countTry;
    private static JTextField text;
    private static JFrame frame;
    private static Image cover;
    private static List<Image> picture = new ArrayList<>();

    /**
     * Read all images for the button.
     * @throws IOException if any image is not exists.
     */
    private static void readImage() throws IOException {
        String location = "img/tp-4/cover.png";
        Image img = ImageIO.read(new File(location));
        cover = img.getScaledInstance(100, 100, Image.SCALE_SMOOTH);

        for (int i = 0; i < 18; i++) {
            location = "img/tp-4/" + i + ".png";
            img = ImageIO.read(new File(location));
            picture.add(img.getScaledInstance(100, 100, Image.SCALE_SMOOTH));
        }
    }

    /**
     * Prepare all the components needed.
     */
    private static void createAndShow() {
        frame = new JFrame("Pair Matching");
        // dispose is a "thread safe" version of exit
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JPanel mainPanel = new JPanel(new BorderLayout());

        try {
            readImage();
            // ButtonPanel should notify this class if number of try changes
            ButtonPanel buttonPanel = new ButtonPanel(e -> {
                countTry++;
                text.setText("Number of Tries: " + countTry);
            }, cover, picture);
            mainPanel.add(buttonPanel, BorderLayout.PAGE_START);

            // prepare other buttons
            JPanel actionPanel = new JPanel(new FlowLayout());
            JButton restartButton = new JButton("Restart");
            restartButton.addActionListener(e -> {
                buttonPanel.prepareAll();
                buttonPanel.revalidate();
                countTry = 0;
                text.setText("Number of Tries: " + countTry);
            });
            JButton exitButton = new JButton("Exit");
            exitButton.addActionListener(e -> frame.dispose());
            actionPanel.add(restartButton);
            actionPanel.add(exitButton);
            mainPanel.add(actionPanel, BorderLayout.CENTER);

            countTry = 0;
            text = new JTextField("Number of Tries: " + countTry);
            text.setHorizontalAlignment(JTextField.CENTER);
            mainPanel.add(text, BorderLayout.PAGE_END);
        } catch (IOException e) {
            JTextField text = new JTextField("Fail to open images");
            mainPanel.add(text);
        }
        frame.getContentPane().add(mainPanel);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Need to call it like this to make it thread safe
        SwingUtilities.invokeLater(PairMatching::createAndShow);
    }
}