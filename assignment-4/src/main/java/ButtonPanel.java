import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * A class that contains the logic behind the game.
 */
class ButtonPanel extends JPanel {
    private Image cover;
    private List<Image> picture;
    private List<Integer> position;
    private List<Button> clicked;
    private Button[][] button;
    private ActionListener incCountTry;

    /**
     * Contructor of Button Panel.
     * @param actionListener actionListener to tell the main class the number of tries.
     */
    ButtonPanel(ActionListener actionListener, Image cover, List<Image> picture) {
        this.cover = cover;
        this.picture = picture;
        clicked = new ArrayList<>();
        position = new ArrayList<>();
        this.incCountTry = actionListener;
        prepareAll();
    }

    /**
     * Randomly shuffle the picture position.
     */
    private void preparePosition() {
        position.clear();
        for (int i = 0; i < 18; i++) {
            position.add(i);
            position.add(i);
        }
        Collections.shuffle(position);
    }

    /**
     * Prepare all the positioning after all images are loaded.
     */
    void prepareAll() {
        if (button != null) {
            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 6; j++) {
                    remove(button[i][j]);
                }
            }
        }

        preparePosition();
        setLayout(new GridLayout(6, 6));
        button = new Button[6][6];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                button[i][j] = new Button(position.get(6 * i + j), new ImageIcon(cover),
                    new ImageIcon(picture.get(position.get(6 * i + j))),
                    e -> {
                        clicked.add((Button)e.getSource());
                        if (clicked.size() == 2) {
                            Button btn1 = clicked.get(0);
                            Button btn2 = clicked.get(1);
                            if (btn1.getNumber() == btn2.getNumber()) {
                                // handle clicking the same button
                                if (btn1 != btn2) {
                                    Timer timer = new Timer(300, ev -> {
                                        btn1.setVisible(false);
                                        btn2.setVisible(false);
                                        clicked.clear();
                                    });
                                    timer.setRepeats(false);
                                    timer.start();
                                    this.incCountTry.actionPerformed(e);
                                } else {
                                    clicked.clear();
                                }
                            } else {
                                // if open the same button, count
                                this.incCountTry.actionPerformed(e);
                            }
                        } else if (clicked.size() == 3) {
                            Button btn1 = clicked.get(0);
                            Button btn2 = clicked.get(1);
                            btn1.toggleImage();
                            btn2.toggleImage();
                            clicked.remove(0);
                            clicked.remove(0);
                        }
                    });
                add(button[i][j]);
            }
        }
    }
}
